/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-non-null-assertion */
import * as k8s from "@kubernetes/client-node";
//import * as fs from "fs";
//const k8s = require("@kubernetes/client-node");
//const fs = require("fs");

// Configure the operator to deploy your custom resources
// and the destination namespace for your pods
const SECRETTEMPLATE_GROUP = "k8s.olsitec.de";
const SECRETTEMPLATE_VERSION = "v1";
const SECRETTEMPLATE_PLURAL = "secrettemplates";

const ClusterSecretTemplates = {
  entries: [],
};
//const namespace = "test";

// This value specifies the amount of pods that your deployment will have
interface SecretTemplateSpec {
  templateConfigMap: string;
  sourceSecret: string;
}

interface SecretTemplate {
  apiVersion: string;
  kind: string;
  metadata: k8s.V1ObjectMeta;
  spec?: SecretTemplateSpec;
}

// Generates a client from an existing kubeconfig whether in memory
// or from a file.
const kc = new k8s.KubeConfig();
kc.loadFromDefault();

// Creates the different clients for the different parts of the API.
//const k8sApi = kc.makeApiClient(k8s.AppsV1Api);
const k8sApiSecret = kc.makeApiClient(k8s.CoreV1Api);
const k8sApiMC = kc.makeApiClient(k8s.CustomObjectsApi);

// This is to listen for events or notifications and act accordingly
// after all it is the core part of a controller or operator to
// watch or observe, compare and reconcile
const watch = new k8s.Watch(kc);

// Then this function determines what flow needs to happen
// Create, Update or Destroy?
async function onSecretTemplateEvent(phase: string, apiObj: any) {
  log(
    `Received onSecretTemplateEvent in phase ${phase}. ${apiObj.metadata.name}`
  );
  if (phase == "ADDED") {
    scheduleReconcile(apiObj);
  } else if (phase == "MODIFIED") {
    try {
      scheduleReconcile(apiObj);
    } catch (err) {
      log(err);
    }
  } else if (phase == "DELETED") {
    await deleteResource(apiObj);
  } else {
    log(`Unknown event type: ${phase}`);
  }
  return true;
}
// Then this function determines what flow needs to happen
// Create, Update or Destroy?
async function onConfigMapEvent(phase: string, apiObj: any) {
  let affectedSecretTemplates = ClusterSecretTemplates.entries.filter(
    (entry) =>
      entry.metadata.namespace == apiObj.metadata.namespace &&
      entry.spec.templateConfigMap == apiObj.metadata.name
  );
  if (affectedSecretTemplates.length == 0) {
    return true;
  }
  log(`Received onConfigMapEvent in phase ${phase}.`);
  if (phase != "DELETED") {
    return Promise.all(
      affectedSecretTemplates.map((affectedSecretTemplate) => {
        return scheduleReconcile(affectedSecretTemplate);
      })
    );
  }
  return true;
}
// Then this function determines what flow needs to happen
// Create, Update or Destroy?
async function onSecretEvent(phase: string, apiObj: any) {
  let affectedSecretTemplates = ClusterSecretTemplates.entries.filter(
    (entry) =>
      entry.metadata.namespace == apiObj.metadata.namespace &&
      entry.spec.sourceSecret == apiObj.metadata.name
  );
  if (affectedSecretTemplates.length == 0) {
    return true;
  }
  log(`Received onSecretEvent in phase ${phase}.`);
  if (phase != "DELETED") {
    return Promise.all(
      affectedSecretTemplates.map((affectedSecretTemplate) => {
        return scheduleReconcile(affectedSecretTemplate);
      })
    );
  }
  return true;
}

// Call the API to destroy the resource, happens when the CRD instance is deleted.
async function deleteResource(obj: SecretTemplate) {
  log(`Deleting ${obj.metadata.namespace}/${obj.metadata.name}`);
  try {
    //await k8sApiMC.deleteNamespacedCustomObject(
    //  SECRETTEMPLATE_GROUP,
    //  SECRETTEMPLATE_VERSION,
    //  SECRETTEMPLATE_PLURAL,
    //  obj.metadata.namespace,
    //  obj.metadata.name
    //);
    await k8sApiSecret.deleteNamespacedSecret(
      obj.metadata.name!,
      obj.metadata.namespace!
    );

    log(`Deleted ${obj.metadata.namespace}/${obj.metadata.name}`);
    ClusterSecretTemplates.entries = ClusterSecretTemplates.entries.filter(
      (entry) =>
        entry.metadata.name != obj.metadata.name &&
        entry.metadata.namespace != obj.metadata.namespace
    );
  } catch (err) {
    log("Unexpected error in deleteResource:" + JSON.stringify(err));
    //log(err);
  }
  return true;
}

// Helpers to continue watching after an event
function onDone(err: any) {
  log(`Connection closed. ${err}`);
  watchResource();
}

async function watchConfigmap(): Promise<any> {
  log("Watching API (configMap)");
  return watch.watch(`/api/v1/configmaps`, {}, onConfigMapEvent, onDone);
}

async function watchSecret(): Promise<any> {
  log("Watching API (secret)");
  return watch.watch(`/api/v1/secrets`, {}, onSecretEvent, onDone);
}

async function watchResource(): Promise<any> {
  log("Watching API (secretTemplate)");
  return watch.watch(
    `/apis/${SECRETTEMPLATE_GROUP}/${SECRETTEMPLATE_VERSION}/${SECRETTEMPLATE_PLURAL}`,
    {},
    onSecretTemplateEvent,
    onDone
  );
}

let reconcileScheduled = false;

// Keep the controller checking every 1000 ms
// If after any condition the controller needs to be stopped
// it can be done by setting reconcileScheduled to true
function scheduleReconcile(obj: SecretTemplate) {
  if (!reconcileScheduled) {
    setTimeout(reconcileNow, 1000, obj);
    reconcileScheduled = true;
  }
}
async function getSecretTemplateResources() {
  const secretTemplateListResponse = await k8sApiMC.listClusterCustomObject(
    SECRETTEMPLATE_GROUP,
    SECRETTEMPLATE_VERSION,
    SECRETTEMPLATE_PLURAL
  );
  const secretTemplateList = secretTemplateListResponse.body;
  //log(`secretTemplates: ` + JSON.stringify(secretTemplateList));

  await Promise.all(
    secretTemplateList["items"].map(async (item: any) => {
      const secretTemplateResponse = await k8sApiMC.getNamespacedCustomObject(
        SECRETTEMPLATE_GROUP,
        SECRETTEMPLATE_VERSION,
        item.metadata.namespace,
        SECRETTEMPLATE_PLURAL,
        item.metadata.name
      );
      const secretTemplate = secretTemplateResponse.body;
      ClusterSecretTemplates.entries.push(secretTemplate);
    })
  );
}

async function reconcileNow(obj: SecretTemplate) {
  reconcileScheduled = false;
  const secretTemplateName: string = obj.metadata.name!;
  const namespace: string = obj.metadata.namespace!;
  await getSecretTemplateResources();
  log(`reconciling ${secretTemplateName}`);
  try {
    log(
      `will try to patch an existing secret ${namespace}/${secretTemplateName} with: ` +
        JSON.stringify({
          sourceSecret: obj.spec!.sourceSecret,
          templateConfigMap: obj.spec!.templateConfigMap,
        })
    );
    const secretResponse = await k8sApiSecret.readNamespacedSecret(
      secretTemplateName,
      namespace
    );
    const sourceSecretResponse = await k8sApiSecret.readNamespacedSecret(
      obj.spec!.sourceSecret,
      namespace
    );
    const configMapResponse = await k8sApiSecret.readNamespacedConfigMap(
      obj.spec!.templateConfigMap,
      namespace
    );
    const secret: k8s.V1Secret = secretResponse.body;
    const sourceSecret: k8s.V1Secret = sourceSecretResponse.body;
    const configMap: k8s.V1ConfigMap = configMapResponse.body;
    let configMapDataJSON = JSON.stringify(configMap.data);
    //console.log(`read configMap content`, configMapDataJSON);
    Object.keys(sourceSecret.data!).forEach((secretStringName) => {
      configMapDataJSON = configMapDataJSON.replace(
        `<<##${secretStringName}##>>`,
        atob(sourceSecret.data[secretStringName])
      );
    });
    //console.log(`patched configMap content`, configMapDataJSON);
    let updatedSecret: k8s.V1Secret = secret;
    updatedSecret.stringData = JSON.parse(configMapDataJSON);

    log(`patched secret ${namespace}/${secretTemplateName}`);
    k8sApiSecret.replaceNamespacedSecret(
      secretTemplateName,
      namespace,
      updatedSecret
    );
    return;
  } catch (err) {
    log("An unexpected error occurred...:" + JSON.stringify(err));
    //log(err);
  }

  // Create the deployment if it doesn't exists
  try {
    log(
      `will try to create a new secret ${namespace}/${secretTemplateName} with: ` +
        JSON.stringify({
          sourceSecret: obj.spec!.sourceSecret,
          templateConfigMap: obj.spec!.templateConfigMap,
        })
    );

    const sourceSecretResponse = await k8sApiSecret.readNamespacedSecret(
      obj.spec!.sourceSecret,
      namespace
    );
    log(`read sourceSecret ${namespace}/${obj.spec!.sourceSecret}`);

    const configMapResponse = await k8sApiSecret.readNamespacedConfigMap(
      obj.spec!.templateConfigMap,
      namespace
    );
    log(`read templateConfigMap ${namespace}/${obj.spec!.templateConfigMap}`);

    const sourceSecret: k8s.V1Secret = sourceSecretResponse.body;
    const configMap: k8s.V1ConfigMap = configMapResponse.body;

    let newSecret: k8s.V1Secret = {
      apiVersion: "v1",
      kind: "Secret",
      metadata: {
        name: "new-secrettemplate",
        namespace: namespace,
      },
      stringData: {},
      type: "Opaque",
    };

    newSecret.metadata!.name = secretTemplateName;
    newSecret.metadata!.annotations = {
      "secrettemplates.k8s.olsitec.de/secrettemplate": "true",
      "secrettemplates.k8s.olsitec.de/sourceSecret": obj.spec!.sourceSecret,
      "secrettemplates.k8s.olsitec.de/templateConfigMap":
        obj.spec!.templateConfigMap,
    };
    newSecret.metadata!.ownerReferences = [
      {
        apiVersion: `${SECRETTEMPLATE_GROUP}/${SECRETTEMPLATE_VERSION}`,
        controller: true,
        kind: `SecretTemplate`,
        name: obj.metadata.name,
        uid: obj.metadata.uid,
      },
    ];

    let configMapDataJSON = JSON.stringify(configMap.data);

    Object.keys(sourceSecret.data!).forEach((secretStringName) => {
      configMapDataJSON = configMapDataJSON.replace(
        `<<##${secretStringName}##>>`,
        atob(sourceSecret.data![secretStringName])
      );
    });

    newSecret.stringData = JSON.parse(configMapDataJSON);
    log(`creating new secret ${namespace}/${secretTemplateName}`);
    k8sApiSecret.createNamespacedSecret(namespace, newSecret);
  } catch (err) {
    log(
      "Unexpected error creating new secret from template.:" +
        JSON.stringify(err)
    );
    //log(err);
  }

  //set the status of our resource to the list of pod names.
  const status: SecretTemplate = {
    apiVersion: obj.apiVersion,
    kind: obj.kind,
    metadata: {
      name: obj.metadata.name!,
      resourceVersion: obj.metadata.resourceVersion,
    },
  };

  try {
    k8sApiMC.replaceNamespacedCustomObjectStatus(
      SECRETTEMPLATE_GROUP,
      SECRETTEMPLATE_VERSION,
      namespace,
      SECRETTEMPLATE_PLURAL,
      obj.metadata.name!,
      status
    );
  } catch (err) {
    log("Unexpected error replacing secretTemplate:" + JSON.stringify(err));
  }
}
// The watch has begun
async function main() {
  await getSecretTemplateResources();
  await watchResource();
  await watchSecret();
  await watchConfigmap();
}

// Helper to pretty print logs
function log(message: string) {
  console.log(`${new Date().toLocaleString()}: ${message}`);
}

// Helper to get better errors if we miss any promise rejection.
process.on("unhandledRejection", (reason, p) => {
  console.log("Unhandled Rejection at: Promise", p, "reason:", reason);
});

// Run
main();
